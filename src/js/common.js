const navBarBtnIcon = document.querySelector(".drop-down-btn_icon");
navBarBtnIcon.addEventListener("touchstart", function() {
    event.preventDefault();
    navBarBtnIcon.classList.toggle("drop-down-btn_icon--active");
    if (document.querySelector(".vertical_item--active")) {
        document.querySelector(".vertical_item--active").classList.toggle("vertical_item--active");
    }
    document.querySelector(".header-menu-vertical-nav-bar").classList.toggle("header-menu-vertical-nav-bar--active");
}, false);

const verticalNavBar = document.querySelector(".header-menu-vertical-nav-bar");
verticalNavBar.addEventListener("touchstart", function (event) {
    event.preventDefault();
    if (event.target.className === "vertical_item") {
        if (document.querySelector(".vertical_item--active")) {
            document.querySelector(".vertical_item--active").classList.toggle("vertical_item--active");
        }
        event.target.classList.toggle("vertical_item--active");
    }
}, false);

const downloadBtn = document.querySelector(".download_btn");
downloadBtn.addEventListener("touchstart", function () {
    event.preventDefault();
    downloadBtn.classList.toggle("download_btn--active");
},false);

const headerLogo = document.querySelector(".header-menu-logo");
headerLogo.addEventListener("touchstart", function (event) {
    event.preventDefault();
    if (event.target.className.includes("logo_img") || event.target.className.includes("logo_text")) {
        document.querySelector(".logo_img").classList.toggle("logo_img--active");
        document.querySelector(".logo_text").classList.toggle("logo_text--active");
    }
},false);

const navBarBtn = document.querySelector(".nav-bar-btn");
navBarBtn.addEventListener("touchstart", function () {
    event.preventDefault();
    navBarBtn.classList.toggle("nav-bar-btn--active")
},false);

const headerMenuNavBar = document.querySelector(".header-menu-nav-bar");
headerMenuNavBar.addEventListener("touchstart", function (event) {
    event.preventDefault();
    if (event.target.className === "nav-bar_item") {
        if (document.querySelector(".nav-bar_item--active")) {
            document.querySelector(".nav-bar_item--active").classList.toggle("nav-bar_item--active");
        }
        event.target.classList.toggle("nav-bar_item--active");
    }
},false);

const pricingBtn = document.querySelector(".pricing-item-container");
pricingBtn.addEventListener("touchstart", function (event) {
    event.preventDefault();
    if (event.target.className === "pricing-item_btn") {
        if (document.querySelector(".pricing-item_btn--active")) {
            document.querySelector(".pricing-item_btn--active").classList.toggle("pricing-item_btn--active");
        }
        event.target.classList.toggle("pricing-item_btn--active");
    }
},false);