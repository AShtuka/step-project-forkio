# step-project-forkio

## About Step-project-forkio

Step project is made as part of a training course in DAN.IT education training center.
The main task of the project is to study how to work in a team of two novice programmers аnd to get necessary skills for such task.
Step-project-forkio is an exmaple of website with adaptive and responsive design 320px, 768px and 1200px.   
This project manages its dependencies using npm. 
This project is build usind package builder Gulp. 
Preprocessor SASS (SCSS) is used.  

Header and section "People Are talking About Fork" is done by Dmytro. 
Sections "Revolutionary Editor", "Here is what you get" and Fork Subscription Pricing is done by Oleksandr.  

## The latest version

Details of the last version can be found on the gitlab repository https://gitlab.com/AShtuka/step-project-forkio.git

## Liсence

Step-project-forkio is open-source software licensed under the ISC License.

## Contacts 

You can find us in Slack chat: 
Oleksandr Shtuka  @ Shtuka Oleksandr
Dmytro Boriskin @ dmytro